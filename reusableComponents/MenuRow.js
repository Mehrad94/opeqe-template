import React from 'react';
import Slider from 'react-slick';
const MenuRow = ({ menuData }) => {
  const settings = {
    slidesToShow: 3.08,
    slidesToScroll: 3,
    lazyLoad: true,
    infinite: false,
  };
  return (
    <div className='menu-row-container'>
      <div className='menu-row'>
        <h2>{menuData.title}</h2>
        <Slider {...settings}>
          {menuData.data.map((h) => {
            return (
              <div className='slider-image'>
                <img src={h.image} />
                <h3>{h.title}</h3>
                <h4>
                  <span>{h.category}</span>
                  <span>{h.nationality}</span>
                  <span>{h.meal}</span>
                </h4>
                <div className='row-footer'>
                  <h5>
                    <i className='fal fa-stopwatch' />
                    <span className='time'>{h.time}</span>
                    <span className='price'>{h.price}</span>
                  </h5>
                  <h4>{h.tag}</h4>
                </div>
              </div>
            );
          })}
        </Slider>
      </div>
    </div>
  );
};

export default MenuRow;
