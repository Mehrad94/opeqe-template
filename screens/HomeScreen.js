import React from 'react';
import Header from '../components/Header';
import MenuRow from '../reusableComponents/MenuRow';
import specialOffers from '../values/specialOffers.json';
import salad from '../values/salad.json';
import appetizer from '../values/appetizer.json';
import mainCourse from '../values/mainCourse.json';
import american from '../values/american.json';
import ApplicationIntroducer from '../components/ApplicationIntroducer';
import Footer from '../components/Footer';
const HomeScreen = () => {
  return (
    <div>
      <Header />
      <MenuRow menuData={specialOffers} />
      <MenuRow menuData={salad} />
      <MenuRow menuData={appetizer} />
      <MenuRow menuData={mainCourse} />
      <ApplicationIntroducer />
      <MenuRow menuData={american} />
      <Footer />
    </div>
  );
};

export default HomeScreen;
