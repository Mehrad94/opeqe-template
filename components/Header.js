import React from 'react';
import logo from '../public/images/png/logo.png';
import basket from '../public/images/png/basket.png';
const Header = () => {
  return (
    <div className='header-container'>
      <section>
        <span className='header-left'>
          <img src={logo} alt='opeqe' />
        </span>

        <span className='header-right'>
          <a>Reservation</a>
          <a>Orders</a>
          <a>Locations</a>
          <div>
            <a className='header-login'>
              <span>Log In</span>
            </a>
          </div>
          <div>
            <a className='header-sign-up'>
              <span>Sign Up</span>
            </a>
          </div>
          <img src={basket} alt='opeqe' />
        </span>
      </section>
    </div>
  );
};

export default Header;
