import React from 'react';
import footer from '../public/images/png/footer.png';
import playStore from '../public/images/png/playStore.png';
import appStore from '../public/images/png/appStroe.png';
import logo from '../public/images/png/logo-w.png';
const Footer = () => {
  return (
    <div className='footer-container'>
      <div className='footer'>
        <div className='footer-image'>
          <img src={footer} alt='footer' />
        </div>
        <div className='first-section'>
          <section className='download-link'>
            <img src={appStore} alt='appStore' />
            <img src={playStore} alt='playStore' />
          </section>
          <section className='footer-menu'>
            <span>About</span>
            <span>Service</span>
            <span>Support</span>
            <span>Gallery</span>
            <span>Terms</span>
            <span>Locations</span>
          </section>
        </div>
        <div className='second-section'>
          <section>
            <div className='footer-logo'>
              <div>
                <span>Powered By</span>
                <img src={logo} alt='logo' />
              </div>
            </div>
          </section>
          <section>
            <h1>Main Menu</h1>
            <span>Pickup</span>
          </section>
          <section>
            <h1>Orders</h1>
            <span>Upcoming Orders</span>
            <span>Recent Orders</span>
          </section>
          <section>
            <h1>Reservation</h1>
            <span>Recent Reservation</span>
            <span>Wait To Be Seated</span>
          </section>
          <section>
            <h1>Profile</h1>
            <span>Promos & Credits</span>
            <span>Rewards</span>
          </section>
          <section>
            <h1>Special Offers</h1>
            <span>Chief Special</span>
            <span>Breakfast Special</span>
          </section>
          <section>
            <h1>Support</h1>
            <span>Contact Us</span>
            <span>Live Chat</span>
          </section>
        </div>
        <div className='third-section'>
          <span>Delight customers everywhere with a branded custom-built native iOS, native Android and Installable Website Application.</span>
          <span>Opeqe is reliable, fast and commission free all-in-one ordering solutions for multi-location or single location restaurants. </span>
          <div className='footer-copyright'>
            <div className='left-part'>
              <span>©2019 OPEQE INC</span>
              <h4>Terms & Conditions</h4>
              <h4>Privacy Policy</h4>
            </div>
            <div className='right-part'>
              <i class='fab fa-instagram' />
              <i class='fab fa-twitter' />
              <i class='fab fa-facebook-square' />
              <i class='fab fa-youtube' />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
