import React from 'react';
import leftImage from '../public/images/jpg/left-application-image.jpg';
import rightImage from '../public/images/jpg/right-application-image.jpg';
const ApplicationIntroducer = () => {
  return (
    <div className='application-container'>
      <div className='application-introducer'>
        <img src={leftImage} alt='application' />
        <div className='middle-section'>
          <h2>CROSS PLATFORM</h2>
          <p>
            native <strong>MOBILE APPLICATION</strong>
          </p>
          <h3>Android, iOS</h3>
          <h3>HYBRID DESIGN Mobile first</h3>
          <h3>INSATIABLE WEB APPLICATION</h3>
          <h3>FOR ANY SIZE RESTAURANT</h3>
        </div>
        <img src={rightImage} alt='application' />
      </div>
    </div>
  );
};

export default ApplicationIntroducer;
