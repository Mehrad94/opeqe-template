import App from 'next/app';
import Head from 'next/head';
import '../public/styles/index.scss';
import '../public/fonts/trebuchet/style.css';
class Opeqe extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <div>
        <Head>
          <title>Opeqe Web App Demo</title>
          <meta name='description' content='Best website in the world' />
          <meta content='width=device-width, initial-scale=1' name='viewport' />
          <link rel='stylesheet' type='text/css' charset='UTF-8' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css' />
          <link rel='stylesheet' type='text/css' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css' />
        </Head>
        <div>
          <Component {...pageProps} />
        </div>
      </div>
    );
  }
}

export default Opeqe;
